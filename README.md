# laravel56



#### 介绍



通过参数的个数的不同可以返回前段想要的数据



【一维数组 （file && value && like）||（file && value）】
  
{
    “chengyugushi_id”:48153,
    “chengyugushi_title”:“争长论短”,
    “chengyugushi_description”:“成语出处就罢手。（清 李宝嘉《文明小史》第五回）”,
    “chengyu_id”:24425,
}



【二维数组 （比一维多一个limit参数）】


[
    {
             “chengyugushi_id”:48153,
             “chengyugushi_title”:“争长论短”,
             “chengyugushi_description”:“成语出处：宋 柳开《穆夫人墓志铭》：“异性相聚，争长竞短。”
     
         }
         
]



【三维数组 比二维数组多一个page参数】



 “current_page”:1,
     “data”:[
         {
             “chengyugushi_id”:48153,
             “chengyugushi_title”:“争长论短”,
             “chengyugushi_description”:“成语出员如此一说，也就罢手。（清 李宝嘉《文明小史》第五回）”,
         }
     ],
     “first_page_url”:“http://www.lara58.com/index.php/api/webGetMiYu?page=1“,
     “from”:1,
     “last_page”:4612,
     “last_page_url”:“http://www.lara58.com/index.php/api/webGetMiYu?page=4612“,
     “next_page_url”:“http://www.lara58.com/index.php/api/webGetMiYu?page=2“,
     “path”:“http://www.lara58.com/index.php/api/webGetMiYu“,
     “per_page”:“1”,
     “prev_page_url”:null,
     “to”:1,
     “total”:4612
 }
 
 【字段关联】
   
   参数1 是数组
   参数2 是数组要参与关联的字段
   参数3 要关联chengyu表的字段
   参数4 要查询chengyu表中的字段
   参数5 给查出来的chengyu字段加上前缀
   最后从新拼接数组 return
   
   $chengyuInfo=Chengyu::Relation($arrayfo,'arrayid','id',$select="id,title",$tableasli="chengyu_");
 
