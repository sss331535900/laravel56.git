<?php


namespace App;


use Illuminate\Database\Eloquent\Model;

/**
 * system login name Administrator
 * Autor             LAOSONG
 * Time              2019/9/5 14:11
 * IDE name          PhpStorm
 * project name      laravel5.6
 */
class ChengyuMiyu extends Model
{
    protected $table='chengyu_miyu';
    public static $tableinfo='chengyu_miyu';

//获取单表信息

    /**
     * @Todo         获取所有数据
     * @Author       LaoSong
     * @Static       static
     * @Data         2019/9/5 12:23
     * @Access       public
     * @Since        1.0
     * @Return       array
     * @Param mixed  $parm 数组参数
     */
    public static function getInfos($parm){
        return  parent::getinfosparent($parm,new self(),self::$tableinfo);
    }

//关联数据表

    /**
     * @Todo
     * @Author       LaoSong
     * @Static       static
     * @Data         2019/9/5 12:14
     * @Access       public
     * @Since        1.0
     * @Return       array
     * @Param mixed  $arrayinfo  //需要关联的数组
     * @Param mixed  $arrayfile  //数组的关联字段 A
     * @Param mixed  $tablefile  //数据表的关联字段 B->A
     * @Param mixed  $select     //select 需要查询的字段 默认是 *（全部）
     * @Param mixed  $tableasli  //表的别名 用来防止关联的时候字段冲突
     */
    public static function Relation($arrayinfo,$arrayfile,$tablefile,$select="*",$tableasli="b"){
        return  parent::relativetableModel(new self,$arrayinfo,$arrayfile,$tablefile,$select,$tableasli);
    }
}